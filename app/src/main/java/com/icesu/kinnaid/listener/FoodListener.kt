package com.icesu.kinnaid.listener

interface FoodListener {
    fun onDeleteFoodClicked(position: Int)
}