package com.icesu.kinnaid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.icesu.kinnaid.base.Config.foodEliminateLast
import com.icesu.kinnaid.base.Config.foodResultPref
import com.icesu.kinnaid.base.Config.playmode
import com.icesu.kinnaid.databinding.ActivityShowResultBinding
import com.icesu.kinnaid.utils.Playmode
import com.icesu.kinnaid.utils.Utils.gone
import com.icesu.kinnaid.utils.Utils.visible

class ShowResultActivity : AppCompatActivity() {

    private val binding: ActivityShowResultBinding by lazy {
        ActivityShowResultBinding.inflate(
            layoutInflater
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val foodResult = intent.getStringExtra(foodResultPref)
        val foodEliminateLast = intent.getStringExtra(foodEliminateLast)
        val playMode = intent.getIntExtra(playmode, 0)

        if (foodEliminateLast.isNullOrEmpty()) {
            binding.lavCelebrate.gone()
            binding.lavRejected.gone()
            when (playMode) {
                Playmode.RANDOM.ordinal -> {
                    binding.lavCelebrate.visible()
                    binding.tvResultTitle.text = resources.getString(R.string.your_food_is)
                }

                Playmode.ELIMINATE.ordinal -> {
                    binding.lavRejected.visible()
                    binding.tvResultTitle.text =
                        resources.getString(R.string.your_food_is_eliminate)
                }

                else -> {
                    binding.lavCelebrate.visible()
                    binding.tvResultTitle.text = resources.getString(R.string.your_food_is)
                }
            }
        } else {
            binding.lavCelebrate.visible()
            binding.tvResultTitle.text = resources.getString(R.string.your_food_is)
        }

        binding.foodResult.text =
            if (foodEliminateLast.isNullOrEmpty()) foodResult else foodEliminateLast

        binding.spinAgain.setOnClickListener {
            finish()
        }
    }
}