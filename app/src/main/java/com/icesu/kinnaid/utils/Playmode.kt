package com.icesu.kinnaid.utils

enum class Playmode {
    RANDOM,
    ELIMINATE,
}