package com.icesu.kinnaid.utils

import android.content.Context
import android.view.View
import android.widget.Toast

object Utils {
    private var currentToast: Toast? = null

    fun View.visible() {
        this.visibility = View.VISIBLE
    }

    fun View.gone() {
        this.visibility = View.GONE
    }

    fun showToast(context: Context, message: String) {
        currentToast?.cancel()
        currentToast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
        currentToast?.show()
    }
}