package com.icesu.kinnaid.base

object Config {
    var isTH = 0
    val settingsPref = "Settings"
    val myLanguagePref = "My_Lang"
    val foodResultPref = "food_result"
    val foodNamePref = "foodName preferences"
    val foodNameListPref = "foodName list"
    val playmode = "play_mode"
    val foodEliminateLast = "food_eliminate_last"
}