package com.icesu.kinnaid.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.icesu.kinnaid.databinding.ItemFoodBinding
import com.icesu.kinnaid.listener.FoodListener

class FoodAdapter : RecyclerView.Adapter<FoodViewHolder>() {

    private var foodList: ArrayList<String> = ArrayList()
    private var listenner: FoodListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val binding = ItemFoodBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return FoodViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        val itemPosition = foodList[holder.adapterPosition]
        holder.apply {
            txtFoodName.text = itemPosition

            btnFoodDelete.setOnClickListener {
                listenner?.onDeleteFoodClicked(adapterPosition)
            }
        }
    }

    override fun getItemCount(): Int {
        foodList.let {
            return it.size
        }
        return 0
    }

    fun setOrder(food: ArrayList<String>) {
        foodList = food
        notifyDataSetChanged()
    }

    fun setListener(listener: FoodListener) {
        this.listenner = listener
    }
}

class FoodViewHolder(binding: ItemFoodBinding) : RecyclerView.ViewHolder(binding.root) {
    val txtFoodName = binding.txtfood
    val btnFoodDelete = binding.foodDelete
}
