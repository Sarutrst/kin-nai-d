package com.icesu.kinnaid

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.icesu.kinnaid.adapter.FoodAdapter
import com.icesu.kinnaid.base.BaseActivity
import com.icesu.kinnaid.base.Config.foodEliminateLast
import com.icesu.kinnaid.base.Config.foodNameListPref
import com.icesu.kinnaid.base.Config.foodNamePref
import com.icesu.kinnaid.base.Config.foodResultPref
import com.icesu.kinnaid.base.Config.isTH
import com.icesu.kinnaid.base.Config.myLanguagePref
import com.icesu.kinnaid.base.Config.playmode
import com.icesu.kinnaid.base.Config.settingsPref
import com.icesu.kinnaid.databinding.ActivityMainBinding
import com.icesu.kinnaid.listener.FoodListener
import com.icesu.kinnaid.utils.Playmode
import com.icesu.kinnaid.utils.Utils.gone
import com.icesu.kinnaid.utils.Utils.showToast
import com.icesu.kinnaid.utils.Utils.visible
import java.lang.reflect.Type
import java.util.Locale
import java.util.Random

class MainActivity : BaseActivity(), FoodListener {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(
            layoutInflater
        )
    }

    private val foodAdapter: FoodAdapter by lazy {
        FoodAdapter()
    }

    private var foodOrderList: ArrayList<String>? = ArrayList()

    private var PLAY_MODE = 0

    private val handlerTimer = 3000L
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (binding.pbLoading.isVisible) {
                    return
                } else {
                    remove()
                    onBackPressedDispatcher.onBackPressed()
                }
            }
        })

        //initRecyclerView
        binding.rcvFood.setHasFixedSize(true)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rcvFood.layoutManager = linearLayoutManager

        //Load data from preference
        loadDataPreference()

        binding.addFood.setOnClickListener {
            val food = binding.edtAddfood.text.toString().trim()
            if (food.isNotBlank()) {
                if (foodOrderList?.contains(food) == true) {
                    showToast(this, resources.getString(R.string.exist_food_order, food))
                    return@setOnClickListener
                }
                foodOrderList?.add(food)
                foodAdapter?.notifyDataSetChanged()

                //Save to Preference
                saveDataPreference()
                showToast(this, resources.getString(R.string.add_food_order, food))
                binding.edtAddfood.setText("")
            }
        }

        binding.btnSpinFood.setOnClickListener {
            val food = binding.edtAddfood.text.toString().trim()
            if (binding.edtAddfood.text.toString().trim().isNotBlank()) {
                if (foodOrderList?.contains(food) == true) {
                    showToast(this, resources.getString(R.string.exist_food_order, food))
                    return@setOnClickListener
                }
                foodOrderList?.add(food)
                foodAdapter?.notifyDataSetChanged()

                //Save to Preference
                saveDataPreference()
                showToast(this, resources.getString(R.string.add_food_order, food))
                binding.edtAddfood.setText("")

                Handler(Looper.getMainLooper()).postDelayed({
                    spinFood()
                }, 500)
            } else {
                spinFood()
            }
        }

        binding.changeLang.setOnClickListener {
            showChangeLang()
        }

        binding.btnPlayModeRandom.setOnClickListener {
            PLAY_MODE = Playmode.RANDOM.ordinal
            binding.btnPlayModeEliminate.setBackgroundResource(R.drawable.bg_unselect)
            binding.btnPlayModeEliminate.setTextColor(ContextCompat.getColor(this, R.color.grey))

            binding.btnPlayModeRandom.setBackgroundResource(R.drawable.bg_selected)
            binding.btnPlayModeRandom.setTextColor(ContextCompat.getColor(this, R.color.white))
            showToast(this, resources.getString(R.string.random_mode_selected))
        }

        binding.btnPlayModeEliminate.setOnClickListener {
            PLAY_MODE = Playmode.ELIMINATE.ordinal
            binding.btnPlayModeRandom.setBackgroundResource(R.drawable.bg_unselect)
            binding.btnPlayModeRandom.setTextColor(ContextCompat.getColor(this, R.color.grey))

            binding.btnPlayModeEliminate.setBackgroundResource(R.drawable.bg_selected)
            binding.btnPlayModeEliminate.setTextColor(ContextCompat.getColor(this, R.color.white))
            showToast(this, resources.getString(R.string.eliminate_mode_selected))
        }
    }

    private fun spinFood() {
        if (foodOrderList?.size!! >= 2) {
            when (PLAY_MODE) {
                Playmode.RANDOM.ordinal -> {
                    binding.root.clearFocus()
                    binding.btnSpinFood.isEnabled = false
                    showLoading()
                    handler.postDelayed({
                        binding.btnSpinFood.isEnabled = true
                        hideLoading()
                        val randomAnswer = getRandomNumberInRange()
                        val foodPosition = foodOrderList?.get(randomAnswer)

                        val intent = Intent(this, ShowResultActivity::class.java)
                        intent.putExtra(playmode, PLAY_MODE)
                        intent.putExtra(foodResultPref, foodPosition)
                        startActivity(intent)
                    }, handlerTimer)
                }

                Playmode.ELIMINATE.ordinal -> {
                    binding.root.clearFocus()
                    binding.btnSpinFood.isEnabled = false
                    showLoading()
                    handler.postDelayed({
                        binding.btnSpinFood.isEnabled = true
                        hideLoading()
                        val randomAnswer = getRandomNumberInRange()
                        val foodPosition = foodOrderList?.get(randomAnswer)
                        val foodPositionIndex = foodOrderList?.indexOf(foodPosition)
                        if (foodPositionIndex != null) {
                            foodOrderList?.removeAt(foodPositionIndex)
                            foodAdapter.notifyDataSetChanged()
                            //Save to Preference
                            saveDataPreference()
                        }

                        val intent = Intent(this, ShowResultActivity::class.java)
                        intent.putExtra(playmode, PLAY_MODE)
                        if (foodOrderList?.size == 1) {
                            intent.putExtra(foodEliminateLast, foodOrderList?.get(0))
                        }
                        intent.putExtra(foodResultPref, foodPosition)
                        startActivity(intent)
                    }, handlerTimer)
                }
            }
        } else {
            showToast(this, resources.getString(R.string.add_more_one_item))
        }
    }

    private fun showLoading() {
        binding.pbLoading.visible()
    }

    private fun hideLoading() {
        binding.pbLoading.gone()
    }

    override fun onBackPressed() {
        onBackPressedDispatcher.onBackPressed()
    }

    private fun saveDataPreference() {
        if (foodOrderList!!.isNotEmpty()) {
            val sharedPreferences: SharedPreferences =
                getSharedPreferences(foodNamePref, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            val gson = Gson()
            val json: String = gson.toJson(foodOrderList)
            editor.putString(foodNameListPref, json)
            editor.apply()
        } else {
            val sharedPreferences: SharedPreferences =
                getSharedPreferences(foodNamePref, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = sharedPreferences.edit()
            editor.clear()
            editor.apply()
        }
    }

    private fun loadDataPreference() {
        val sharedPreferences = getSharedPreferences(foodNamePref, Context.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPreferences.getString(foodNameListPref, null)
        val type: Type = object : TypeToken<ArrayList<String>>() {}.type
        foodOrderList = gson.fromJson<ArrayList<String>>(json, type)

        if (foodOrderList == null) {
            foodOrderList = ArrayList()
        }
        foodAdapter.setListener(this)
        foodAdapter.setOrder(foodOrderList!!)
        binding.rcvFood.adapter = foodAdapter
    }

    private fun getRandomNumberInRange(): Int {
        return Random().nextInt(foodOrderList?.size ?: 0)
    }

    override fun onDestroy() {
        handler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

    private fun showChangeLang() {
        val listLang = arrayOf("ไทย", "English")
        val mBuilder = AlertDialog.Builder(this@MainActivity)
        mBuilder.setTitle(resources.getString(R.string.choose_lang))
        mBuilder.setSingleChoiceItems(listLang, isTH) { dialog, which ->
            when (which) {
                0 -> {
                    setLocate("th")
                    recreate()
                }

                1 -> {
                    setLocate("en")
                    recreate()
                }
            }
            dialog.dismiss()
        }
        val mDialog = mBuilder.create()
        mDialog.show()
    }

    private fun setLocate(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()

        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)

        val editor = getSharedPreferences(settingsPref, Context.MODE_PRIVATE).edit()
        editor.putString(myLanguagePref, lang)
        editor.apply()
    }

    override fun onDeleteFoodClicked(position: Int) {
        val foodFromIndex = foodOrderList?.get(position)
        foodOrderList?.removeAt(position)
        foodAdapter.notifyDataSetChanged()
        showToast(this, resources.getString(R.string.remove_food_order, foodFromIndex))

        //Save to Preference
        saveDataPreference()
    }

}