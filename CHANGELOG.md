# Changelog

## [1.0.5](https://gitlab.com/icesarut/kin-nai-d/-/releases/1.0.5) - 2024-09-27

* Randomly choose what to eat from your list of food.


## [1.0.7](https://gitlab.com/icesarut/kin-nai-d/-/releases/1.0.7) - 2024-10-03

* Added new play mode (Random mode/Eliminate mode)
