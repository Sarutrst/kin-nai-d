## <div align="center">Kin nai D : กินไหนดี</div>

![cover.png](./image/cover.png)

มันยากไหมที่จะตัดสินใจว่าจะกินอะไรเป็นมื้อกลางวัน? ให้ ‘กินไหนดี’ ช่วยคุณเลือกว่าจะกินอะไร!

Is it hard to decide what to eat for lunch? Let 'Kin Nai D' help you choose what you want to eat!

[<img alt="Get it on F-Droid" height="80" src="https://raw.githubusercontent.com/mueller-ma/android-common/main/assets/get-it-on-fdroid.png"/>](https://f-droid.org/en/packages/com.icesu.kinnaid/)[<img alt="Download from GitHub" height="80" src="https://raw.githubusercontent.com/mueller-ma/android-common/main/assets/direct-apk-download.png"/>](https://gitlab.com/icesarut/kin-nai-d/-/releases/1.0.7/downloads/app-release.apk)

[![Kotlin](https://img.shields.io/badge/Kotlin-%237F52FF.svg?logo=kotlin&logoColor=white)](#) [![License: ApacheLicense2.0](https://img.shields.io/badge/License-Apache_License_2.0-green)](https://www.apache.org/licenses/LICENSE-2.0) 

## Screen Shot
![ScreenShotTH](./image/screenshot/kin-nai-d-screenshot-th.png)
![ScreenShotEN](./image/screenshot/kin-nai-d-screenshot-en.png)

## Demo
![Demo](./video/kin_nai_d_demo.mov)
